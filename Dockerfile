FROM python:3.10-bookworm
COPY . /app
WORKDIR /app
# 本地调试镜像加速
# RUN pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip3 install -r requirements.txt
ENV PYTHONPATH=/app
CMD ["python", "/app/main.py"]