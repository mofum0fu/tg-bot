from telebot.types import InputMediaPhoto, InputMediaVideo
from website.site_template import Site


class MessageBuilder:
    def __init__(self, obj: Site):
        self.obj = obj

    @property
    def caption(self):
        return f'<a href="{self.obj.url()}">{str(self.obj.username())}</a>\n' + \
            f'{f"#{str(self.obj.userid())}" if self.obj.userid() else ""}' + \
            f'{" #nsfw" if self.obj.is_nsfw() else ""}'

    def _media_messages(self):
        result = []
        for link in self.obj.media_urls():
            if "video.twimg.com" in link or ".mp4" in link:
                value = self._vid_message(link)
            else:
                value = self._img_message(link)
            value.link = link
            result.append(value)
        return result

    def _img_message(self, link):
        return InputMediaPhoto(media=link)

    def _vid_message(self, link):
        return InputMediaVideo(media=link)

    def single_img_messages(self):
        msg = self._media_messages()
        for m in msg:
            m.caption = self.caption
            m.parse_mode = "html"
        return msg

    def group_img_messages(self):
        result = []
        msgs = self._media_messages()
        for i in range((len(msgs) // 10) + 1):
            group = msgs[10 * i:10 * (1 + i)]
            if len(group) != 0:
                group[0].caption = self.caption
                group[0].parse_mode = "html"
                result.append(group)

        return result
