import json
import os

SAVE_FILE_NAME = "saved_data.json"


def save_after_run(function):
    def wrapper(*args, **kwargs):
        a = function(*args, **kwargs)
        args[0].save()
        return a

    return wrapper


class Store:

    def __init__(self):
        self.big_admin = None
        self._admins = {}
        self.queue = []
        self.publish = None
        self.accept_application = False
        self.pixiv_refresh_token = None
        self.time_gap = 15 * 60
        self.send_count = 5
        self.manage_channel = None

    def load(self):
        if os.path.exists(SAVE_FILE_NAME):
            with open(SAVE_FILE_NAME, "r") as file:
                data = json.load(file)
                self.__dict__.update(data)
        print(self.__dict__)

    def set_big_admin(self, admin_id):
        self.big_admin = admin_id

    def has_admin(self, id):
        id = str(id)
        return id in self.admins

    @property
    def admins(self):
        return list(self._admins.keys()) + [self.big_admin]

    @save_after_run
    def remove_admin(self, id):
        del self._admins[id]

    @save_after_run
    def add_admin(self, id, name):
        self._admins[str(id)] = name

    def save(self):
        with open(SAVE_FILE_NAME, "w+") as file:
            data = json.dumps(self.__dict__, indent=2)
            file.write(data)

