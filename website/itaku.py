import logging
from time import strftime, strptime
import requests
import re

from website.site_template import Site


class Twitter(Site):
    UA = {'User-Agent': 'facebookexternalhit/1.1'}

    def __init__(self, url):
        super().__init__()
        self.raw_info = None
        self._nsfw = False
        self.id = re.findall(r"itaku\.ee/images/(\d+)", url)[0]
        self.original_url = f'https://itaku.ee/api/galleries/images/{self.id}/'


    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*itaku\.ee/.*$", url)

    def type(self):
        if self.raw_info["video"] is not None:
            return "video"
        else:
            return "image"

    def info_request(self):
        try:
            self.raw_info = requests.get(self.original_url, headers=self.UA).json()
        except Exception as e:
            logging.info(f"Boom!{e}")

    def media_urls(self):
        if self.type() == "video":
            return [self.raw_info["video"]["video"]]
        else:
            return [self.raw_info["image"]]

    def is_nsfw(self):
        return False if self.raw_info["maturity_rating"] == "SFW" else True

    def username(self):
        return self.raw_info["owner_username"]

    def userid(self):
        return self.raw_info["owner_displayname"]

    def url(self):
        return f'https://itaku.ee/images/{self.id}'

    def datatime(self):
        utc = self.raw_info["date_added"]
        datatime = strftime('%Y-%m-%d %H:%M:%S', strptime(utc, '%Y-%m-%dT%H:%M:%S.%fZ'))
        return datatime
