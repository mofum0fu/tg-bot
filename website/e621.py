import requests
import re

from website.site_template import Site


class E621(Site):
    UA = {'User-Agent': 'lmao/1.1'}

    def __init__(self, url):
        super().__init__()
        self.raw_info = None
        self.original_url = url
        self.id = re.findall(r"e621\.net/posts/(\d+)", url)[0]

    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*e621\.net/.*$", url)

    def info_request(self):
        self.raw_info = requests.get(f"https://e621.net/posts/{self.id}.json", headers=self.UA).json()

    def media_urls(self):
        if self.raw_info["post"]["file"]["size"] >= 5 * 1024 * 1024:
            return [self.raw_info["post"]["sample"]["url"]]
        return [self.raw_info["post"]["file"]["url"]]

    def is_nsfw(self):
        return self.raw_info["post"]["rating"] == 'e'

    def username(self):
        return self.url()

    def userid(self):
        return None

    def url(self):
        sources = self.raw_info["post"]["sources"]
        return sources[0] if sources else self.original_url
