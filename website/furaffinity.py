import logging
from time import strftime, strptime
import requests
import re

from website.site_template import Site


class Furaffinity(Site):
    UA = {'User-Agent': 'facebookexternalhit/1.1'}

    def __init__(self, url):
        super().__init__()
        self.raw_info = None
        self._nsfw = False
        self.id = re.findall(r"furaffinity\.net/view/(\d+)/", url)[0]
        self.original_url = f"https://faexport.spangle.org.uk/submission/{self.id}.json"

    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*furaffinity\.net/view/.*$", url)

    def type(self):
        return "image"

    def info_request(self):
        try:
            self.raw_info = requests.get(self.original_url, headers=self.UA).json()
        except Exception as e:
            logging.info(f"Boom!{e}")

    def media_urls(self):
        return [self.raw_info["download"]]

    def is_nsfw(self):
        if self.raw_info["rating"] in {"Adult", "Mature"}:
            return True
        return False

    def username(self):
        return self.raw_info["name"]

    def userid(self):
        return self.raw_info["profile_name"]

    def url(self):
        return self.raw_info["link"]

    def datatime(self):
        utc = self.raw_info["posted_at"]
        datatime = strftime('%Y-%m-%d %H:%M:%S', strptime(utc, '%Y-%m-%dT%H:%M:%SZ'))
        return datatime
