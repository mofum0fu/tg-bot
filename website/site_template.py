import json


class Site:
    all_sites = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        Site.all_sites.append(cls)

    def __init__(self):
        self._type = "image"
        self._image_urls = None
        self._video_urls = None
        self._media_urls = None
        self._is_nsfw = False
        self._username = None
        self._userid = None
        self._url = None
        self._datatime = None

    def type(self):
        return self._type

    def info_request(self):
        pass

    def image_urls(self):
        return self._image_urls

    def video_urls(self):
        return self._video_urls

    def media_urls(self):
        return self._media_urls

    def is_nsfw(self):
        return self._is_nsfw

    def username(self):
        return self._username

    def userid(self):
        return self._userid

    def url(self):
        return self._url

    def datatime(self):
        return self._datatime

    def __str__(self):
        return json.dumps({
            "type": self.type(),
            "image_urls": self.image_urls(),
            "video_urls": self._video_urls(),
            "media_urls": self._media_urls(),
            "is_nsfw": self.is_nsfw(),
            "username": self.username(),
            "user_id": self.userid(),
            "url": self.url()
        })
