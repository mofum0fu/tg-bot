import logging
from os import environ
from time import strftime, strptime

from pixivpy3 import AppPixivAPI
import re
from gppt import GetPixivToken

from website.site_template import Site


class Pixiv(Site):
    aapi = None

    def __init__(self, url):
        super().__init__()
        self.raw_info = None
        self.original_url = url
        self.id = re.findall(r"www\.pixiv\.net/artworks/(\d+)", url)[0]

    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*\.pixiv\.net/artworks/.*$", url)

    @staticmethod
    def init_client():
        Pixiv.aapi = AppPixivAPI()
        Pixiv.aapi.auth(refresh_token=environ["REFRESH_TOKEN"])

    def info_request(self):
        self.aapi.auth(refresh_token=environ["REFRESH_TOKEN"])
        self.raw_info = self.aapi.illust_detail(self.id)
        logging.info(self.raw_info)

    def media_urls(self):
        if self.raw_info["illust"]["meta_single_page"]:
            return [self.raw_info["illust"]["image_urls"]["large"]]
        return [image["image_urls"]["large"] for image in self.raw_info["illust"]["meta_pages"]]

    def is_nsfw(self):
        tags = [tag["name"] for tag in self.raw_info['illust']['tags']]
        return "R-18" in tags

    def username(self):
        return self.raw_info['illust']['user']["name"]

    def userid(self):
        return self.raw_info['illust']['user']["name"]

    def url(self):
        return self.original_url

    def datatime(self):
        utc = self.raw_info['illust']['create_date']
        datatime = strftime('%Y-%m-%d %H:%M:%S', strptime(utc, '%Y-%m-%dT%H:%M:%S%z'))
        return datatime
