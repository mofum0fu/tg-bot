import logging
from time import strftime, localtime
import requests
import re

from website.site_template import Site


class Twitter(Site):
    UA = {'User-Agent': 'facebookexternalhit/1.1'}

    def __init__(self, url):
        super().__init__()
        self._from_fx = None
        self.raw_info = None
        self._nsfw = False
        [[_, self._userid, self.id]] = re.findall(r"(twitter|x)\.com/([a-zA-Z0-9_]{1,15})/status/(\d+)", url)
        self.original_url = f"https://api.vxtwitter.com/{self._userid}/status/{self.id}"

    @staticmethod
    def is_valid_url(url):
        return re.match(r"https://(.*twitter|twitter|x)\.com/([a-zA-Z0-9_]{1,15})/status/(\d+)", url)

    def type(self):
        return self.raw_info["media_extended"][0]["type"]

    def info_request(self):
        try:
            self.raw_info = requests.get(self.original_url, headers=self.UA).json()
        except Exception as e:
            logging.info(f"Boom!{e}")

    def media_urls(self):
        urls_return = []
        vx_media_url_orig = self.raw_info["mediaURLs"]
        for url in vx_media_url_orig:
            if ".mp4" in url:
                urls_return.append(url)
            else:
                urls_return.append(f"{url}:large")
        return urls_return

    def is_nsfw(self):
        return self.raw_info["possibly_sensitive"]

    def username(self):
        return self.raw_info["user_name"]

    def userid(self):
        return self.raw_info["user_screen_name"]

    def url(self):
        return self.raw_info["tweetURL"]

    def datatime(self):
        datatime = strftime('%Y-%m-%d %H:%M:%S', localtime(self.raw_info["date_epoch"]))
        return datatime
