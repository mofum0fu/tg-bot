# Setup
## Environment Variables (.env):
```
    ADMIN_ID
    BOT_TOKEN
    REFRESH_TOKEN (PIXIV) (Optional)
    API_USER
    API_PW
    API_BaseUrl
```
## Create saved_data.json
```
    {
      "big_admin": telegram user_id,
      "_admins": {},
      "queue": [],
      "publish": publish channal_id,
      "accept_application": false,
      "pixiv_refresh_token": null,
      "time_gap": 5,
      "send_count": 5,
      "manage_channel": null,
      "recent_sent": [],
      "recent_size": 200
    }
```
## Startup
```
    docker-compose pull
    docker-compose up --build -d
```