from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton


def send_button():
    return InlineKeyboardMarkup([[
        InlineKeyboardButton("Send!", callback_data=f"send_now"),
    ]])


def published_button():
    return InlineKeyboardMarkup([[InlineKeyboardButton("Published!✅", url='https://t.me/mesukemo')]])


def not_published_button():
    return InlineKeyboardMarkup([[InlineKeyboardButton("Go check /queue!❌", url='https://t.me/mesukemo')]])


def pass_button(table_id, again=False):
    return InlineKeyboardMarkup([[
        InlineKeyboardButton("Pass again⁉️" if again else "Pass❔", callback_data=f"2pass_{table_id}"),
    ]])


def recall_button(table_id):
    return InlineKeyboardMarkup([[
        InlineKeyboardButton("Recall⬅️", callback_data=f"2recall_{table_id}"),
    ]])

