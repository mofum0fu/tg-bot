import argparse
import asyncio
import atexit
import logging
import time
from os import environ

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from pixivpy3 import AppPixivAPI
from telebot import asyncio_helper
from telebot.async_telebot import AsyncTeleBot
from telebot.types import Message, CallbackQuery
from telebot.types import InputMediaPhoto, InputMediaVideo

from message_sender.single_msg_sender import SingleMessageSender
from store_json import Store
from website.pixiv import Pixiv
from website.site_template import Site
from button import *
from api import *

# asyncio_helper.proxy = 'http://127.0.0.1:7890'

logging.getLogger().setLevel(logging.INFO)
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--proxy", action='store_true', help="use proxy")
arg_store = parser.parse_args()

bot = AsyncTeleBot(environ["BOT_TOKEN"], parse_mode="html", )
store = Store()

scheduler = AsyncIOScheduler()

drf_api = Api(environ["API_USER"], environ["API_PW"])


@bot.callback_query_handler(func=lambda call: call.data.startswith("send_now") and is_admin(call))
async def send_message_now(callback: CallbackQuery):
    chat_id = callback.message.chat.id
    message_id = callback.message.message_id
    if await send_candidates():
        await bot.edit_message_reply_markup(chat_id, message_id, reply_markup=published_button())
    else:
        await bot.edit_message_reply_markup(chat_id, message_id, reply_markup=not_published_button())


def get_id(string):
    return string.split("_", 1)[1]


def is_admin(message: Message):
    return store.has_admin(message.from_user.id)


def is_big_admin(message: Message):
    return store.has_admin(message.from_user.id)


@bot.callback_query_handler(func=lambda call: call.data.startswith("2pass") and is_admin(call))
async def pass_message(callback: CallbackQuery):
    chat_id = callback.message.chat.id
    message_id = callback.message.message_id
    table_id = get_id(callback.data)
    await drf_api.login_or_refresh()
    await drf_api.put_post(table_id, True)
    await bot.edit_message_reply_markup(chat_id, message_id, reply_markup=recall_button(table_id))


@bot.callback_query_handler(func=lambda call: call.data.startswith("2recall") and is_admin(call))
async def recall_message(callback: CallbackQuery):
    chat_id = callback.message.chat.id
    message_id = callback.message.message_id
    table_id = get_id(callback.data)
    markup = pass_button(table_id, False)
    await drf_api.login_or_refresh()
    await drf_api.put_post(table_id, False)
    await bot.edit_message_reply_markup(chat_id, message_id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data.startswith("admin_"))
async def pass_admin(callback: CallbackQuery):
    [_, id, name] = callback.data.split("_", 2)
    store.add_admin(id, name)
    await bot.edit_message_reply_markup(chat_id=callback.message.chat.id, message_id=callback.message.id)


@bot.message_handler(commands=['updatetoken'], func=is_big_admin)
async def command(message: Message):
    logging.info(message.text)
    [_, refresh_key] = message.text.split()
    Pixiv.aapi = AppPixivAPI()
    Pixiv.aapi.auth(refresh_token=refresh_key)
    await bot.send_message(chat_id=message.chat.id, text="New token set.")


@bot.message_handler(commands=['refresh'], func=is_big_admin)
async def command(message: Message):
    logging.info(message.text)
    Pixiv.aapi = AppPixivAPI()
    Pixiv.aapi.auth(refresh_token=environ["REFRESH_TOKEN"])
    await bot.send_message(chat_id=message.chat.id, text="Token refreshed.")


@bot.message_handler(commands=['be_admin'])
async def command(message: Message):
    if store.has_admin(message.from_user.id):
        await bot.send_message(text="You have already been an admin.", reply_to_message_id=message.message_id,
                               chat_id=message.chat.id)
        return

    text = f'''
    Admin Application
    id: {message.from_user.id}
    username: {message.from_user.username}
    name: {message.from_user.last_name} {message.from_user.first_name}
    '''
    await bot.send_message(chat_id=store.big_admin, text=text, reply_markup=InlineKeyboardMarkup([[
        InlineKeyboardButton("Pass",
                             callback_data=f"admin_{message.from_user.id}_{message.from_user.last_name} {message.from_user.first_name}"),
    ]]))


@bot.message_handler(commands=['list_admin'], func=is_big_admin)
async def command(message: Message):
    await bot.send_message(chat_id=message.chat.id,
                           text="\n".join([f"{x} {store._admins[x]}" for x in store._admins]) if len(
                               store._admins) else "No other admins.")


@bot.message_handler(commands=['remove_admin'], func=is_big_admin)
async def command(message: Message):
    store.remove_admin(message.text.split()[1])


@bot.message_handler(commands=['set_manage_channel'], func=is_big_admin)
async def command(message: Message):
    store.manage_channel = message.chat.id
    await bot.send_message(chat_id=store.manage_channel, text=f"manage channel set to this: {store.manage_channel}")


@bot.message_handler(commands=['set_publish_channel'], func=is_big_admin)
async def command(message: Message):
    store.publish = message.text.split(" ")[1]
    await bot.send_message(chat_id=store.publish, text=f"publish channel set to this: {store.publish}")


@bot.message_handler(commands=['pass'], func=is_admin)
async def commands(message: Message):
    chat_id = message.chat.id
    await SingleMessageSender(bot, chat_id, message.text, drf_api, True).send(directly_pass=True)


@bot.message_handler(commands=['send_now'], func=is_big_admin)
async def command(message: Message):
    await bot.send_message(chat_id=message.chat.id, text='⚠Send now?', reply_markup=send_button())


@bot.message_handler(commands=['queue'], func=is_admin)
async def command(message: Message):
    chat_id = message.chat.id
    num = await drf_api.count()
    await bot.send_message(chat_id, f"queue:{num}")


def has_valid_url(message):
    for link in message.text.split():
        for site in Site.all_sites:
            if site.is_valid_url(link):
                return True


@bot.message_handler(func=lambda message: not message.text.startswith("/") and has_valid_url(message))
async def formatter(message: Message):
    chat_id = message.chat.id
    await SingleMessageSender(bot, chat_id, message.text, drf_api).send()


async def send_candidates():
    logging.info(f"try to send.")
    await drf_api.login_or_refresh()

    # 组图
    if store.publish and await drf_api.can_send():
        logging.info(f"sending.")
        for i in await drf_api.pop(await drf_api.number_to_send()):
            a = 0
            media = []
            content_type = i["content_type"]
            caption = f'<a href="{i["url"]}">{str(i["username"])}</a>\n' + \
                      f'#{str(i["userid"])}' + \
                      f'{" #nsfw" if i["nsfw"] else ""}'
            try:
                for j in i["media_url"]:
                    if a == 0:
                        if "video" in content_type:
                            msg = InputMediaVideo(media=str(j), caption=caption, parse_mode="html")
                        else:
                            msg = InputMediaPhoto(media=str(j), caption=caption, parse_mode="html")
                        a = a + 1
                    else:
                        if "video" in content_type:
                            msg = InputMediaVideo(media=str(j))
                        else:
                            msg = InputMediaPhoto(media=str(j))
                    media.append(msg)
                    if len(i["media_url"]) != 1:
                        await drf_api.set_published_media_url(j)
                    else:
                        await drf_api.set_published_id(i["id"])
                await bot.send_media_group(
                    chat_id=store.publish,
                    media=media,
                )
                time.sleep(1.5)
            except Exception as e:
                a = 0
                media.clear()
                for j in i["media_url"]:
                    if a == 0:
                        if "video" in content_type:
                            msg = InputMediaVideo(media=str(j), caption=caption, parse_mode="html")
                        else:
                            msg = InputMediaPhoto(media=str(j)+":small", caption=caption, parse_mode="html")
                        a = a + 1
                    else:
                        if "video" in content_type:
                            msg = InputMediaVideo(media=str(j))
                        else:
                            msg = InputMediaPhoto(media=str(j))
                    media.append(msg)
                    if len(i["media_url"]) != 1:
                        await drf_api.set_published_media_url(j)
                    else:
                        await drf_api.set_published_id(i["id"])
                await bot.send_media_group(
                    chat_id=store.publish,
                    media=media,
                )
                time.sleep(1.5)
        return True
    else:
        logging.info(f"group msgs not send.")


def on_exit():
    store.save()


async def post_task():
    # scheduler.add_job(send_candidates, "interval", seconds=60)
    scheduler.add_job(send_candidates, "cron", hour=2, timezone="Atlantic/Reykjavik", id="utc02")
    scheduler.add_job(send_candidates, "cron", hour=11, timezone="Atlantic/Reykjavik", id="utc11")
    scheduler.add_job(send_candidates, "cron", hour=19, timezone="Atlantic/Reykjavik", id="utc19")
    scheduler.add_job(send_candidates, "cron", hour=20, timezone="Atlantic/Reykjavik", id="utc20")
    scheduler.start()


async def run_task():
    store.load()
    store.set_big_admin(int(environ.get("ADMIN_ID")))
    atexit.register(on_exit)

    # if environ.get("REFRESH_TOKEN"):
    #     Pixiv.init_client()
    task_1 = drf_api.login()
    task_2 = post_task()
    task_3 = bot.polling()
    await asyncio.gather(task_1, task_2, task_3)

if __name__ == '__main__':
    asyncio.run(run_task())
