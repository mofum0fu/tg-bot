from button import *
from message_sender.message_sender import MessageSender
from telebot.apihelper import ApiTelegramException


class SingleMessageSender(MessageSender):
    def __init__(self, bot, chat_id, raw_urls, api, directly_pass=False):
        super().__init__(bot, chat_id, raw_urls, api)
        self.directly_pass = directly_pass

    async def send_msg_api(self, obj):
        for item in obj:

            # Caption
            caption = f'<a href="{item["article"]}">{str(item["artist_nickname"])}</a>\n' + \
                      f'#{str(item["artist_social_id"])}' + \
                      f'{" #nsfw" if item["note"]["nsfw"] else ""}'

            # markup按键
            if item["approved"] or self.directly_pass:
                markup = recall_button(item["id"])
            else:
                markup = pass_button(item["id"], False)

            # 发送
            if "video" in item["mime"]:
                message = await self.bot.send_video(
                    chat_id=self.chat_id,
                    video=item["media_url"],
                    caption=caption,
                    reply_markup=markup
                )
            else:
                try:
                    message = await self.bot.send_photo(
                        chat_id=self.chat_id,
                        photo=item["media_url"],
                        caption=caption,
                        reply_markup=markup
                    )
                except Exception as e:
                    message = await self.bot.send_photo(
                        chat_id=self.chat_id,
                        photo=item["media_url"] + ":small",
                        caption=caption,
                        reply_markup=markup
                    )
