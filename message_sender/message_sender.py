import logging

from message_builder import MessageBuilder
from processing import processing


class MessageSender:
    def __init__(self, bot, chat_id, raw_urls, api):
        self.obj = None
        self.error = False
        self.base_msg_id = None
        self.msg_builder = None
        self.bot = bot
        self.api = api
        self.chat_id = chat_id
        self.current_index = -1
        self.link_list = list(set(raw_urls.split()))
        logging.info(f"incoming {len(self.link_list)} links")

    async def send_msg(self, obj):
        pass

    async def send_msg_api(self, obj):
        pass

    @property
    def current_url(self):
        return self.link_list[self.current_index]

    def has_next(self):
        self.current_index += 1
        return self.current_index < len(self.link_list)

    def process_this_url(self):
        self.obj = processing(self.current_url)
        self.msg_builder = MessageBuilder(self.obj)

    async def on_error(self, e):
        await self.bot.edit_message_text(chat_id=self.chat_id, message_id=self.base_msg_id,
                                         text=f"⚠️Boom! {str(e)}")
        self.error = True
        logging.warning(f"Boom! {str(e)} ")

    async def before_sending_message(self):
        self.base_msg_id = (await self.bot.send_message(self.chat_id, "Processing🤔")).message_id

    async def before_sending_this(self):
        pass

    async def after_sending(self):
        pass

    async def clean_up(self):
        if not self.error:
            await self.bot.delete_message(self.chat_id, self.base_msg_id)

    async def send(self, directly_pass=False):
        await self.api.login_or_refresh()
        await self.before_sending_message()
        while self.has_next():
            try:
                await self.before_sending_this()
                response = await self.api.add_post(self.current_url, directly_pass)
                await self.send_msg_api(response)
                await self.after_sending()
            except Exception as e:
                await self.on_error(e)
        await self.clean_up()
