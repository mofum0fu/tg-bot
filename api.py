import aiohttp
from os import environ
import logging


baseUrl = str(environ.get("API_BaseUrl"))


class Api:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.access = None
        self.refresh = None

    async def login(self):
        data = {
            "username": self.username,
            "password": self.password
        }
        headers = {
            'Content-Type': 'application/json'
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(
                    url=f'{baseUrl}api/jwtauth/token/',
                    headers=headers,
                    json=data
            ) as resp:
                self.access = (await resp.json())["access"]
                self.refresh = (await resp.json())["refresh"]

    async def refresh_access_token(self):
        data = {
            "refresh": self.refresh
        }
        headers = {
            'Content-Type': 'application/json'
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(
                    url=f'{baseUrl}api/jwtauth/refresh/',
                    headers=headers,
                    json=data
            ) as resp:
                json = await resp.json()
                logging.info(json)
                if json.get("access"):
                    logging.info("No ValueError raised.")
                    self.access = json["access"]
                else:
                    logging.info("Token outdated, raised ValueError.")
                    raise ValueError('Refresh token outdated.')

    async def login_or_refresh(self):
        try:
            await self.refresh_access_token()
        except ValueError:
            await self.login()

    async def add_post(self, message, directly_pass):
        urls = list(set(message.split()))
        if '/pass' in urls:
            urls.pop(0)
        data = {
            "url": urls
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access}',
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(
                url=f'{baseUrl}api/submission/?directpass=true' if directly_pass else f'{baseUrl}api/submission/',
                headers=headers,
                json=data
            ) as resp:
                return await resp.json()

    async def put_post(self, itemsId, status):
        data = {
            "approved": status
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access}',
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(url=f'{baseUrl}api/media/?id={itemsId}', headers=headers, json=data) as resp:
                return await resp.json()

    async def count(self):
        headers = {
            'Content-Type': 'application/json',
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(
                    url=f'{baseUrl}api/media/?approved=true&published=false&size=1000',
                    headers=headers
            ) as resp:
                return (await resp.json())['count']

    async def can_send(self):
        return await self.count() > 0

    async def pop(self, size=5):

        size = size if size <= 36 else 36

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access}',
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(
                    url=f'{baseUrl}api/media/?approved=true&published=false',
                    headers=headers
            ) as resp:
                orig_dict_list = (await resp.json())['results']

                # 组图url合并处理
                merged_data = {'url': []}
                merged_list = []
                for item in orig_dict_list:
                    if item['article'] in merged_data['url']:
                        merged_data["media_url"].append(item['media_url'])
                        continue
                    else:
                        merged_data = {
                            'id': item['id'],
                            'username': item['artist_nickname'],
                            'userid': item['artist_social_id'],
                            'url': item['article'],
                            'media_url': [item['media_url']],
                            'nsfw': item['note']['nsfw'],
                            'content_type': item['mime'],
                        }
                    merged_list.append(merged_data)
                return merged_list[0:size]

    async def number_to_send(self):
        num = await self.count()
        if num < 5:
            return num
        if num // 5 > 5:
            return num // 5
        return 5

    async def set_published_media_url(self, media_url):
        data = {
            "note": {
                "published": True
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access}',
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(
                url=f'{baseUrl}api/media/?media_url={media_url}',
                headers=headers,
            ) as resp:
                itemid = (await resp.json())["results"][0]["id"]

            async with session.post(
                url=f'{baseUrl}api/media/?id={itemid}',
                headers=headers,
                json=data
            ) as resp:
                return await resp.json()

    async def set_published_id(self, itemId):
        data = {
            "note": {
                "published": True
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access}',
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(
                url=f'{baseUrl}api/media/?id={itemId}',
                headers=headers,
                json=data
            ) as resp:
                return await resp.json()
